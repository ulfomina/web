let host = "http://exam-2020-1-api.std-900.ist.mospolytech.ru/api/data1";
let filledData = get_data();

function get_data() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", host, false);
    xhr.send();
    if (xhr.status != 200) {
        alert(xhr.status + ": " + xhr.statusText);
    } else {
        return JSON.parse(xhr.response);
    }
}

// ДАННЫЕ ДЯЛ ПАГИНАЦИИ
var state = {
    querySet: get_data(),
    page: 1,
    rows: 10,
    window: 5,
};

// ПРИ ЗАГРУЗКЕ СТРАНИЦЫ ПОДГРУЖАЕТСЯ СПИСОК ЗАВЕДЕНИЙ
window.onload = function() {
    get_host();
};

// ДЛЯ ПАГИНАЦИИ, ВЫЧИСЛЕНИЕ ПЕРЕКЛЮЧЕНИЯ СТРАНИЦЫ С ПОСЛЕДУЮЩИМ ОТСЕЧЕНИЕМ ДАННЫХ
function pagination(querySet, page, rows) {
    var trimStart = (page - 1) * rows;
    var trimEnd = trimStart + rows;
    var trimmedData = querySet.slice(trimStart, trimEnd);
    var pages = Math.round(querySet.length / rows);//округление в целую
    return {
        querySet: trimmedData,
        pages: pages,
    };
}

// ГЕНЕРАЦИЯ КНОПОК ДЛЯ ПАГИНАЦИИ
function pageButtons(pages) {
    var wrapper = document.getElementById("page");
    wrapper.innerHTML = ``;
    var maxLeft = state.page - Math.floor(state.window / 2);
    var maxRight = state.page + Math.floor(state.window / 2);
    if (maxLeft < 1) {
        maxLeft = 1;
        maxRight = state.window;
    }
    if (maxRight > pages) {
        maxLeft = pages - (state.window - 1);
        if (maxLeft < 1) {
            maxLeft = 1;
        }
        maxRight = pages;
    }
    for (var page = maxLeft; page <= maxRight; page++) {
        wrapper.innerHTML += `<button value=${page} class="page btn btn-sm btn-info">${page}</button>`;
    }
    if (state.page != 1) {
        wrapper.innerHTML =
            `<button value=${1} class="page btn btn-sm btn-info">&#171; First</button>` +
            wrapper.innerHTML;
    }
    if (state.page != pages) {
        wrapper.innerHTML += `<button value=${pages} class="page btn btn-sm btn-info">Last &#187;</button>`;
    }
    // ОЧИСТКА СТРАНИЦЫ ПЕРЕД СЛЕДУЮЩИМ ПЕРЕХОДОМ
    $(".page").on("click", function() {
        $(".choice").empty();
        state.page = Number($(this).val());
        get_host();
    });
}

// ФУНКЦИЯ ГЕНЕРАЦИЯ ДАННЫХ НА СТРАНИЦЫ
function get_host() {
    var data = pagination(state.querySet, state.page, state.rows);
    var myList = data.querySet;
    let list = document.querySelector(".choice");
    for (var i = 1 in myList)//проходим циклом по массиву объектов
     { 
        list.innerHTML += `<tr>
                        <th scope="row">${myList[i].name}</th>
                        <td>${myList[i].typeObject}</td>
                        <td>${myList[i].address}</td>
                        <td>
                        <button id="example_2_1" type="button" class="btn btn-dark col-md-12" onclick="document.getElementById('showMenu').style.display='block'; document.getElementById('example_2_1').style.display='none'; document.getElementById('example_2_2').style.display='block';" style="display:none;">Выбрать</button>
                        <button class="btn btn-dark col-md-12"  id="example_2_2" onclick=" document.getElementById('showMenu').style.display='none'; document.getElementById('example_2_2').style.display='none'; document.getElementById('example_2_1').style.display='block';">Скрыть</button>
                      </td>
                      </tr>`;
    }
    pageButtons(data.pages);
}

function filter() {
    let data = $("#filter_form").serializeArray(); //преобразут в массив объектов, упорядочивая их
    console.log(data);
    let new_meta = filledData;
    let arr1 = [];
    let arr2 = [];
    let arr3 = [];
    let arr4 = [];

    for (var i = 0; i < new_meta.length; i++) {
        if (data[0]["value"] != "" && new_meta[i]["admArea"] == data[0]["value"]) {
            arr1.push(new_meta[i]);
        } else if (data[0]["value"] == "") {
            arr1.push(new_meta[i]);
        }
    }

    for (var i = 0; i < arr1.length; i++) {
        if (data[1]["value"] != "" && arr1[i]["district"] == data[1]["value"]) {
            arr2.push(arr1[i]);
        } else if (data[1]["value"] == "") {
            arr2.push(arr1[i]);
        }
    }
    for (var i = 0; i < arr2.length; i++) {
        if (
            data[3]["value"] != "" &&
            arr2[i]["socialPrivileges"] == data[3]["value"]
        ) {
            arr3.push(arr2[i]);
        } else if (data[3]["value"] == "") {
            arr3.push(arr2[i]);
        }
    }
    for (var i = 0; i < arr3.length; i++) {
        if (data[2]["value"] != "" && arr3[i]["typeObject"] == data[2]["value"]) {
            arr4.push(arr3[i]);
        } else if (data[2]["value"] == "") {
            arr4.push(arr3[i]);
        }
    }
    state.querySet = arr4;
    state.page = 1;
    state.rows = 10;
    state.window = 5;
    let list = document.querySelector(".choice");
    list.innerHTML = ``;
    get_host();

    event.preventDefault();
}

filledAdm();
filledDist();
filledType();

// заполнение адмистративный округ
function filledAdm() {
    let data = filledData;
    let adm = document.querySelector("#adm");
    var array = [];

    for (var i = 0; i < data.length; i++) {
        if (!array.includes(data[i]["admArea"])) {//возвращает тру если находит данный элемент
            array.push(data[i]["admArea"]);
            //console.log(data[i]["admArea"]);
            adm.innerHTML += `<option selected value = "${data[i]["admArea"]}">${data[i]["admArea"]}</option>`;
        }
    }
}

// заполнение района
function filledDist() {
    let data = filledData;
    let adm = document.querySelector("#dis");
    console.dir(adm);
    var array = [];

    for (var i = 0; i < data.length; i++) {
        if (!array.includes(data[i]["district"])) {
            array.push(data[i]["district"]);
            //console.log(data[i]["district"]);
            adm.innerHTML += `<option selected value = "${data[i]["district"]}">${data[i]["district"]}</option>`;
        }
    }
}

// заполнение типов
function filledType() {
    let data = filledData;
    let adm = document.querySelector("#types");
    console.dir(adm);
    var array = [];

    for (var i = 0; i < data.length; i++) {
        if (!array.includes(data[i]["typeObject"])) {
            array.push(data[i]["typeObject"]);
            //console.log(data[i]["typeObject"]);
            adm.innerHTML += `<option selected value = "${data[i]["typeObject"]}">${data[i]["typeObject"]}</option>`;
        }
    }
}

let menu = [
    {
    "name": "Пицца с веченой и грибами",
    "picture": "/fotEat/1.jpg",
    "description": "Грибы и ветчина на вкуснейшем сыре ",
    "amount": "0"
    },
   {
    "name": "Фруктовая пицца ",
    "picture": "/fotEat/2.jpg",
    "description": "ягоды , фрукты и варенье в одном блюде",
    "amount": "0"
    },
    {
    "name": "Темный Бургер",
    "picture": "/fotEat/3.jpg",
    "description": "Вкуснейший бургер с говяжей котлетой , плавленным сыром и соусом",
    "amount": "0"
    },
    {
    "name": "Свинной бургер",
    "picture": "/fotEat/4.jpg",
    "description": "Вкуснейшая котлета на мягких и свежих булочках",
    "amount": "0"
    },
    {
    "name": "Суши Калифорния + Филадельфия",
    "picture": "/fotEat/5.jpg",
    "description": "Вкуснейшая калифорния со свежей рыбой и приятная Филадельфия ",
    "amount": "0"
    },
    {
    "name": "Нагетсы с картошкой фри”",
    "picture": "/fotEat/6.jpg",
    "description": "Куриные нагетсы с соленой и хрустящей картошкой ",
    "amount": "0"
    },
    {
    "name": "Большой сет",
    "picture": "/fotEat/7.jpg",
    "description": "1 кг разных наборов роллов ",
    "amount": "0"
    },
    {
    "name": "Супер набор!",
    "picture": "/fotEat/8.jpg",
    "description": "Картошка (150г) + Шаверма(250г) + Напиток(Pepsi)(1,5 литра) ",
    "amount": "0"
    },
    {
    "name": "Чай+чизкейк",
    "picture": "/fotEat/9.jpg",
    "description": "Черный кай и Клубничный чизкейк(300г)",
    "amount": "0"
    },
    {
    "name": "Набор-BIG BOSS  ",
    "picture": "/fotEat/10.jpg",
    "description": "3 пиццы(Пицца Цезарь (30см), Пицца Овощная (30см), Пицца Бекон (30см)) + 2 Колы (1л)",
    "amount": "0"
    },
     {
    "name": "Кола  ",
    "picture": "/fotEat/11.jpg",
    "description": "Вредно для здоровья, берите чай",
    "amount": "0"
    },
     
  
  ];
  createMenu(menu);

function createMenu(menu){
    //let menu = require('./menu.json');

    console.log(menu);
    let table = document.getElementById('myTable');
    for (let i = 0; i < menu.length; i++){
       
        let row = `<tr> 
                        <td class='nameEat'>${menu[i].name}</td>
                        <td></td>
                        <td class='descEat '>${menu[i].description}</td>
                        <td>${menu[i].amount}<br><button class="btn btn-dark col-md-12">Выбрать</button></td>
                   </tr> 
         
        
        `
        table.innerHTML += row;
        table.innerHTML += `<div class="card mt-2 " style="width: 40vh; margin-left: 50vh"; > 
        <img src="./fotEat/fotEat/${Number(i + 1)}.jpg" class="card-img-top" alt="..." />`
        
    
        
    }
   
}



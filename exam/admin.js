let host = "http://exam-2020-1-api.std-900.ist.mospolytech.ru/api/data1";
let filledData = get_data();

function get_data() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", host, false);
    xhr.send();
    if (xhr.status != 200) {
        alert(xhr.status + ": " + xhr.statusText);
    } else {
        return JSON.parse(xhr.response);
    }
}

// ДАННЫЕ ДЯЛ ПАГИНАЦИИ
var state = {
    querySet: get_data(),
    page: 1,
    rows: 10,
    window: 5,
};

// ПРИ ЗАГРУЗКЕ СТРАНИЦЫ ПОДГРУЖАЕТСЯ СПИСОК ЗАВЕДЕНИЙ
window.onload = function() {
    get_host();
};

// ДЛЯ ПАГИНАЦИИ, ВЫЧИСЛЕНИЕ ПЕРЕКЛЮЧЕНИЯ СТРАНИЦЫ С ПОСЛЕДУЮЩИМ ОТСЕЧЕНИЕМ ДАННЫХ
function pagination(querySet, page, rows) {
    var trimStart = (page - 1) * rows;
    var trimEnd = trimStart + rows;
    var trimmedData = querySet.slice(trimStart, trimEnd);
    var pages = Math.round(querySet.length / rows);//округление в целую
    return {
        querySet: trimmedData,
        pages: pages,
    };
}

// ГЕНЕРАЦИЯ КНОПОК ДЛЯ ПАГИНАЦИИ
function pageButtons(pages) {
    var wrapper = document.getElementById("page");
    wrapper.innerHTML = ``;
    var maxLeft = state.page - Math.floor(state.window / 2);
    var maxRight = state.page + Math.floor(state.window / 2);
    if (maxLeft < 1) {
        maxLeft = 1;
        maxRight = state.window;
    }
    if (maxRight > pages) {
        maxLeft = pages - (state.window - 1);
        if (maxLeft < 1) {
            maxLeft = 1;
        }
        maxRight = pages;
    }
    for (var page = maxLeft; page <= maxRight; page++) {
        wrapper.innerHTML += `<button value=${page} class="page btn btn-sm btn-info">${page}</button>`;
    }
    if (state.page != 1) {
        wrapper.innerHTML =
            `<button value=${1} class="page btn btn-sm btn-info">&#171; First</button>` +
            wrapper.innerHTML;
    }
    if (state.page != pages) {
        wrapper.innerHTML += `<button value=${pages} class="page btn btn-sm btn-info">Last &#187;</button>`;
    }
    // ОЧИСТКА СТРАНИЦЫ ПЕРЕД СЛЕДУЮЩИМ ПЕРЕХОДОМ
    $(".page").on("click", function() {
        $(".choice").empty();
        state.page = Number($(this).val());
        get_host();
    });
}

// ФУНКЦИЯ ГЕНЕРАЦИЯ ДАННЫХ НА СТРАНИЦЫ
function get_host() {
    var data = pagination(state.querySet, state.page, state.rows);
    var myList = data.querySet;
    let list = document.querySelector(".choice");
    for (var key = 1 in myList)//проходим циклом по массиву объектов
     { 
        list.innerHTML += `<tr>
                        <th scope="row">${myList[key].name}</th>
                        <td>${myList[key].typeObject}</td>
                        <td>${myList[key].address}</td>
                        <td>
                          <button type="button" class="btn btn-dark col-md-12" onclick = "select_draw(${myList[key].id})">Выбрать</button>
                        </td>
                      </tr>`;
    }
    pageButtons(data.pages);
}

function filter() {
    let data = $("#filter_form").serializeArray(); //преобразут в массив объектов, упорядочивая их
    console.log(data);
    let new_meta = filledData;
    let arr1 = [];
    let arr2 = [];
    let arr3 = [];
    let arr4 = [];

    for (var i = 0; i < new_meta.length; i++) {
        if (data[0]["value"] != "" && new_meta[i]["admArea"] == data[0]["value"]) {
            arr1.push(new_meta[i]);
        } else if (data[0]["value"] == "") {
            arr1.push(new_meta[i]);
        }
    }

    for (var i = 0; i < arr1.length; i++) {
        if (data[1]["value"] != "" && arr1[i]["district"] == data[1]["value"]) {
            arr2.push(arr1[i]);
        } else if (data[1]["value"] == "") {
            arr2.push(arr1[i]);
        }
    }
    for (var i = 0; i < arr2.length; i++) {
        if (
            data[3]["value"] != "" &&
            arr2[i]["socialPrivileges"] == data[3]["value"]
        ) {
            arr3.push(arr2[i]);
        } else if (data[3]["value"] == "") {
            arr3.push(arr2[i]);
        }
    }
    for (var i = 0; i < arr3.length; i++) {
        if (data[2]["value"] != "" && arr3[i]["typeObject"] == data[2]["value"]) {
            arr4.push(arr3[i]);
        } else if (data[2]["value"] == "") {
            arr4.push(arr3[i]);
        }
    }
    state.querySet = arr4;
    state.page = 1;
    state.rows = 10;
    state.window = 5;
    let list = document.querySelector(".choice");
    list.innerHTML = ``;
    get_host();

    event.preventDefault();
}

filledAdm();
filledDist();
filledType();

// заполнение адмистративный округ
function filledAdm() {
    let data = filledData;
    let adm = document.querySelector("#adm");
    var array = [];

    for (var i = 0; i < data.length; i++) {
        if (!array.includes(data[i]["admArea"])) {//возвращает тру если находит данный элемент
            array.push(data[i]["admArea"]);
            //console.log(data[i]["admArea"]);
            adm.innerHTML += `<option selected value = "${data[i]["admArea"]}">${data[i]["admArea"]}</option>`;
        }
    }
}

// заполнение района
function filledDist() {
    let data = filledData;
    let adm = document.querySelector("#dis");
    console.dir(adm);
    var array = [];

    for (var i = 0; i < data.length; i++) {
        if (!array.includes(data[i]["district"])) {
            array.push(data[i]["district"]);
            //console.log(data[i]["district"]);
            adm.innerHTML += `<option selected value = "${data[i]["district"]}">${data[i]["district"]}</option>`;
        }
    }
}

// заполнение типов
function filledType() {
    let data = filledData;
    let adm = document.querySelector("#types");
    console.dir(adm);
    var array = [];

    for (var i = 0; i < data.length; i++) {
        if (!array.includes(data[i]["typeObject"])) {
            array.push(data[i]["typeObject"]);
            //console.log(data[i]["typeObject"]);
            adm.innerHTML += `<option selected value = "${data[i]["typeObject"]}">${data[i]["typeObject"]}</option>`;
        }
    }
}


  
